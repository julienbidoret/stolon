<?php 

$configfile = "config.json";

// create config file if needed
if (file_exists($configfile)) {
  header("Location:" . str_replace('install.php', '', $_SERVER['REQUEST_URI']));
}

// init config object
$json_config = [
  "logins" => [],
  "theme" => "",
  "templates"=> [] 
];  

$available_themes = glob("js/monaco-themes/*.json");

$themes = array_map(function($t){ 
  return str_replace(".json", "", basename($t)); 
}, $available_themes);


// if no user is found in config file, create user and stems dir
if (isset($_POST["new_username"]) ) {
  $u = $_POST['new_username'];
  $p = $_POST['new_password'];
  // hash password
  $pw = password_hash($p, PASSWORD_DEFAULT);
  array_push($json_config["logins"], [$u => $pw]);
  // get theme  
  $json_config['theme'] = $_POST['theme'];
  // get templates
  $templates = array_filter($_POST, function($k){
    return substr( $k, 0, 8 ) === "template";
  });
  foreach ($templates as $tpl => $dir) {
    $json_config["templates"][$tpl] = $dir;
  }
  // save to config file
  $json = json_encode($json_config, JSON_PRETTY_PRINT); 
  $fh = fopen($configfile, 'w') or die("can't write config file");
  fwrite($fh, $json);
  fclose($fh);
  // create stems dir
  if (!file_exists('stems')) {
    mkdir('stems', 0755, true);
  }
  // create download dir
  if (!file_exists('download')) {
    mkdir('download', 0755, true);
  }
  // redirect
  $_SESSION["stolon_username"] = $u;
  setcookie('cansave', true, time()+3600*24, '/', '', false, false);
  header("Location:" . $_SERVER['REQUEST_URI']);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Stolon — install</title>
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="font/stylesheet.css">
  <link rel="stylesheet" href="css/main.css">
  <link href='img/favicon.png' rel='icon' type='image/png'>
</head>
<body>
<div class="create_user">
  <form action="install.php" method="post">
    <h3>Welcome to your own Stolon</h3>
    <fieldset>
      <legend>Create a new account</legend>
      <p><label for="new_username">name</label><input type="text" required="true" name="new_username" id="new_username"></p>
      <p><label for="new_password">password</label><input type="password" required="true" name="new_password" id="new_password"></p>
    </fieldset>  
    <fieldset>
      <legend>Choose default theme</legend>
      <select name="theme" id="theme" class="editable">
        <?php foreach ($themes as $t) :?>
          <option value="<?= $t ?>" <?= $t == "Stolon" ? "selected" : "" ?>><?= $t ?></option>
        <?php endforeach ?>
      </select>
    </fieldset>
    <fieldset>
      <legend>Choose available templates</legend>
        <p>
          <input type="hidden" value="templates/web" name="Web" id="Web">
          <input type="checkbox" disabled="disabled"  checked="true" value="fakeweb" name="fakeweb" id="Web">
          <label for="Web">Web</label>
        </p>
        <p>
          <input type="checkbox" checked="true" value="templates/p5" name="P5.js" id="P5.js">
          <label for="P5.js">P5.js</label>
        </p>
        <p>
          <input type="checkbox" checked="true" value="templates/paged" name="Paged.js" id="Paged.js">
          <label for="Paged.js">Paged.js</label>
        </p>
    </fieldset>
    <p><button type="submit" class="button">ok</button></p></form>
</div>
<div class="overlay"></div>
  
</body>
</html>