const d = document;
const b = d.body;
const titleField = d.querySelector('.title');
const saveBtn = b.querySelector('.save-btn');
const forkBtn = d.querySelector('.fork-btn');
const menuBtn = d.querySelector('.menu-btn');
const templatesBtn = d.querySelector('#templates-dropdown-button');
const templates = d.querySelector('.templates-container');
const layoutBtns = d.querySelectorAll('.layout-line button');
const downloadBtn = d.querySelector('.download-btn');
const toggles = d.querySelectorAll('.toggle');
const fontSlider = d.querySelector('#fs');
const liveReloadInput = d.querySelector('#live-reload');
const themeSelect = d.querySelector('#theme');
let liveReload = b.getAttribute('data-livereload');
const mainForm = d.querySelector('#mainform');
const loginForm = d.querySelector('#login-form');
const loginBtn = d.querySelector('.login-btn');
const cancelLoginBtn = d.querySelector('#cancel-login');
const stolon_username = d.querySelector('#stolon_username');
const saved = b.classList.contains('saved');
const reloadBtn = d.querySelector('.live-line input');
let panesStatus = { html: true, css: true, js: true, output: true };

const htmlPane = d.querySelector('#html-pane');
const cssPane = d.querySelector('#css-pane');
const jsPane = d.querySelector('#js-pane');
const outputPane = d.querySelector('#output-pane');

const editors = {
  html: { editor: null, el: 'html-editor', language: 'html' },
  css: { editor: null, el: 'css-editor', language: 'css' },
  js: { editor: null, el: 'js-editor', language: 'javascript' },
};

let libs = [];

// ------------------------------------------------------------------ Cookies

const cookieTime = 1440; // one day in minutes

d.title = `${d.title} | ${titleField.value}`;

if (Cookies.get('cansave')) {
  b.dataset.cansave = 1;
}

// ------------------------------------------------------------------ Handle screenshot

d.querySelector('#take-screenshot').onclick = () => {
  const cansave = Cookies.get('cansave');
  if (!cansave) {
    loginBtn.click();
    b.classList.remove("menu-opened")
  } else {
    b.classList.add('screenshot');
  }
};

async function uploadFile(file){
  const formData = new FormData();
  formData.append('title', d.querySelector('.title').value);
  formData.append('file', file);
  const response = await fetch("screenshot.php", {
    method: 'POST',
    body: formData,
  });  
  const jsonResponse = await response.json();
  if (jsonResponse.success === 1) {
    b.classList.remove('screenshot');
  }
  console.log(jsonResponse);
  console.info(jsonResponse.messages.join('. '));
};

const uploadBtn = d.getElementById('upload-btn');
uploadBtn.addEventListener('click', (e) => {
  e.preventDefault();
  const { files } = d.querySelector('#screenshot');
  uploadFile(files[0]);
});

// ------------------------------------------------------------------ Login

loginBtn.onclick = (e) => {
  e.preventDefault();
  b.classList.toggle('loging');
  stolon_username.focus();
};
loginForm.onsubmit = (e) => {
  if (mainForm.dataset.loginthensave) {
    e.preventDefault();
    b.classList.remove('loging');
    submitForm();
  }
};
cancelLoginBtn.onclick = (e) => {
  e.preventDefault();
  b.classList.remove('loging');
};

// ------------------------------------------------------------------ Handle config

function getCurrentConfig() {
  const activePanes = Cookies.get('panesStatus')
    ? JSON.parse(Cookies.get('panesStatus'))
    : panesStatus;
  const config = {
    htmlPaneSize: window.getComputedStyle(htmlPane).flexGrow,
    cssPaneSize: window.getComputedStyle(cssPane).flexGrow,
    jsPaneSize: window.getComputedStyle(jsPane).flexGrow,
    outputPaneSize: window.getComputedStyle(outputPane).flexGrow,
    fontSize: fontSlider.value,
    liveReload: liveReloadInput.value,
    layoutMode: b.dataset.layout,
    panes: activePanes,
  };
  return JSON.stringify(config);
}

// can only be triggered after all editors has been initialized
function applyConfig(config) {
  if (config) {
    htmlPane.style.flexGrow = config.htmlPaneSize;
    cssPane.style.flexGrow = config.cssPaneSize;
    jsPane.style.flexGrow = config.jsPaneSize;
    outputPane.style.flexGrow = config.outputPaneSize;
    fontSlider.value = config.fontSize;
    liveReloadInput.value = config.liveReload;
  }
  setPanes();
  triggerFontChange();
  triggerLayoutChange(b.dataset.layout);
}

// ------------------------------------------------------------------ Save = sumbit main form

function saveStem() {
  // check cookie
  const cansave = Cookies.get('cansave');
  if (!cansave) {
    // want to save but not logged in: send info to loginform
    mainForm.dataset.loginthensave = true;
    // then show login form
    b.classList.add('loging');
    stolon_username.focus();
  } else {
    submitForm();
    refreshOutput(0);
  }
}
// form ajax submission
async function submitForm() {
  // type: 'main', 'fork'

  const fd = new FormData(mainForm);

  // gather contents
  const editorsValues = {
    html_content: editors.html.editor.getValue(),
    css_content: editors.css.editor.getValue(),
    js_content: editors.js.editor.getValue(),
  };
  for (const [key, value] of Object.entries(editorsValues)) {
    fd.append(key, value);
  }

  // merge config with contents
  const config = getCurrentConfig();
  fd.append('config', config);
  // merge auth with data if user is not logged in
  if (mainForm.dataset.loginthensave) {
    const authForm_data = new FormData(loginForm);
    for (const i of authForm_data.entries()) {
      fd.append(i[0], i[1]);
    }
  }

  // deal with libs
  if (libs) {
    fd.append("libs", libs);
  }
  // if asking for fork
  if (mainForm.dataset.fork) {
    fd.append('fork', true);
  }

  // fetch
  const response = await fetch(mainForm.action, {
    method: 'POST',
    body: fd,
  });
  const jsonResponse = await response.json();

  console.info(jsonResponse.messages.join('. '));

  if (jsonResponse.success === 1) {
    // update cookie and login status
    Cookies.set('cansave', 1, { expires: cookieTime * 60 });
    b.classList.remove('anonymous');
    b.classList.add('logged');
    // highlight save button
    saveBtn.classList.remove('mark_as_saved');
    void saveBtn.offsetWidth;
    saveBtn.classList.add('mark_as_saved');
    // redirect ?
    if (jsonResponse.redirect != undefined) {
      const newTitle = jsonResponse.redirect;
      window.location.href = newTitle;
    }
  } else {
    alert(
      'Sorry. Something went wrong. Please refer to the console and advise us on gitlab 🙏',
    );
  }

  return false;
}

// ------------------------------------------------------------------ Pane toggles

function setPanes() {
  for (const pane of Object.keys(panesStatus)) {
    const el = d.querySelector(`.${pane}-pane`);
    // if pane is set to false in cookies
    if (!panesStatus[pane]) {
      const toggle = d.querySelector(`.toggle[data-toggle="${pane}"]`);
      toggle.classList.add('hidden');
      el.classList.add('hidden');
    }
  }
}
toggles.forEach((toggle) => {
  toggle.onclick = function () {
    const toToggle = toggle.getAttribute('data-toggle');
    const el = d.querySelector(`.${toToggle}-pane`);
    if (el.classList.contains('hidden')) {
      toggle.classList.remove('hidden');
      el.classList.remove('hidden');
      el.style.flexGrow = 1;
      // update panesStatus array
      panesStatus[toToggle] = true;
    } else {
      toggle.classList.add('hidden');
      el.classList.add('hidden');
      // update panesStatus array
      panesStatus[toToggle] = false;
    }
    Cookies.set('panesStatus', JSON.stringify(panesStatus), {
      expires: cookieTime * 60,
    });
  };
});

// ------------------------------------------------------------------ Keyboard and buttons

d.onkeydown = function (e) {
  // Save: Capture CTRL + S
  if ((e.metaKey || e.ctrlKey) && e.keyCode === 83) {
    saveStem();
    return false;
  }
  // Escape from config, screenshot or login
  if (e.key === 'Escape') {
    b.classList.remove('menu-opened');
    b.classList.remove('screenshot');
    b.classList.remove('loging');
    b.classList.remove('templates-opened');
  }
};

saveBtn.onclick = function () {
  saveStem();
};

forkBtn.onclick = function () {
  const title = d.querySelector('.title').value;
  const forkTitle = d.querySelector('#fork-title').value;
  d.querySelector('.title').value = forkTitle;
  d.querySelector('#derivated-from').value = title;
  // submitForm()
  mainForm.dataset.fork = true;
  saveStem();
  refreshOutput(0);
};

downloadBtn.onclick = function () {
  if (saved) {
    saveStem();
    window.location = this.getAttribute('data-url');
  } else {
    alert('Save your stem first');
  }
};

// menu toggle
menuBtn.onclick = function () {
  if (b.classList.contains('menu-opened')) {
    b.classList.remove('menu-opened');
  } else {
    b.classList.add('menu-opened');
  }
};

// new by template toggle
templatesBtn.onclick = function () {
  if (b.classList.contains('templates-opened')) {
    b.classList.remove('templates-opened');
  } else {
    templates.style.right = window.innerWidth - templatesBtn.getBoundingClientRect().right + "px"
    b.classList.add('templates-opened');
  }
};

// live reload
// @todo: use and set attr on init ?
liveReload = (liveReload === 'true'); // string to boolean
if (liveReload) {
  reloadBtn.setAttribute('checked', 'true');
} else {
  reloadBtn.removeAttribute('checked');
}

reloadBtn.onclick = function () {
  liveReload = !liveReload;
  // set reload state for style
  b.setAttribute('data-livereload', liveReload);
  liveReloadInput.value = liveReload;
};

// theme
themeSelect.onchange = () => {
  setTheme(themeSelect.value);
  Cookies.set('stolon_theme', themeSelect.value);
}

// Font size
fontSlider.onchange = triggerFontChange;
function triggerFontChange() {
  const fz = fontSlider.value;
  const lh = fz * 1.4;
  editors.html.editor.updateOptions({
    fontSize: fz,
    lineHeight: lh
  });
  editors.css.editor.updateOptions({
    fontSize: fz,
    lineHeight: lh
  });
  editors.js.editor.updateOptions({
    fontSize: fz,
    lineHeight: lh
  });
}

// layout
layoutBtns.forEach((btn) => {
  btn.onclick = function () {
    layoutBtns.forEach((bt) => {
      bt.classList.remove('active');
    });
    triggerLayoutChange(btn.dataset.layout);
  };
});

// ------------------------------------------------------------------ Layout

function triggerLayoutChange(layout) {
  b.dataset.layout = layout;
  d.querySelector(`button[data-layout='${layout}']`).classList.add('active');
}

// ------------------------------------------------------------------ Editors

const editorsOptions = {
  autoIndent: 'full',
  contextmenu: false,
  fontFamily: 'jetbrains mono',
  fontSize: fontSlider.value,
  lineHeight: fontSlider.value * 1.4,
  hideCursorInOverviewRuler: true,
  matchBrackets: 'always',
  minimap: {
    enabled: false,
  },
  lineNumbers: 'on',
  scrollbar: {
    horizontalSliderSize: 12,
    verticalSliderSize: 12,
  },
  selectOnLineNumbers: true,
  suggest: {
    snippetsPreventQuickSuggestions: false,
  },
}; 

async function setTheme(theme){
  fetch(`js/monaco-themes/${theme}.json`)
  .then(data => data.json())
  .then(data => {
    monaco.editor.defineTheme('stolon', data);
    monaco.editor.setTheme('stolon');
    return true;    
  })
}
// load Monaco editor
require.config({ paths: { vs: 'js/monaco-editor/min/vs' } });
require(['vs/editor/editor.main'], function() {
  setTheme(theme.value).then(() => {
    initStolon();
  })
});

function initEditor(e, value){
  const options = Object.assign(editorsOptions, {
    value: value,
    language: e.language,
    automaticLayout: true
  });
  e.editor = monaco.editor.create(document.getElementById(e.el), options ); 
}

// ------------------------------------------------------------------ Init
function initStolon(){

  // the stem_dir is set whether from an existing stem json or a template-based json
  fetch(`${b.dataset.stem_dir}/stem.json`)
    .then((response) => response.json())
    .then((json) => {

      // console.debug(json);

      emmetMonaco.emmetHTML()
      initEditor(editors.html, json.html);
      emmetMonaco.emmetCSS()
      initEditor(editors.css, json.css);
      emmetMonaco.emmetJSX()
      initEditor(editors.js, json.js);

      // Read original_stem_config first, or secondly cookies, for initial layout
      if ('config' in json) {
        panesStatus = json.config.panes;
        // console.log(panesStatus);
        b.dataset.layout = json.config.layoutMode;
      } else if (Cookies.get('panesStatus')) {
        panesStatus = JSON.parse(Cookies.get('panesStatus'));
      }
      applyConfig(json.config);

      // handle libs
      if('libs' in json){
        libs = json["libs"];
      }
    });
}

// ------------------------------------------------------------------ Helpers

function refreshOutput(delay = 0) {
  // Delay is used mainly for live reload
  setTimeout(() => {
    const dsrc = d.getElementById('output').getAttribute('data-src');
    d.getElementById('output').src = dsrc;
  }, delay);
}
refreshOutput(0);

// Slugify title
titleField.oninput = function () {
  this.value = slug(this.value, { lower: true, replacement: '-' });
};

// Debounce method
const debounce = function (func, wait, immediate) {
  let timeout;
  return function () {
    const context = this;
    const args = arguments;
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

const debouncedSaveStem = debounce(() => {
  saveStem();
  refreshOutput(50); // 50ms delay to let file writing happen
}, 200);
