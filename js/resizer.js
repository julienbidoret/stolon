function manageResize(mouseDown) {
  const resizer = mouseDown.target;
  if (! resizer.classList.contains("flex-resizer")) {    
    return;
  }

  mouseDown.preventDefault();
  
  // Disable output pane (need due to iframe, reset in onMouseUp)
  const outputpane = document.querySelector("#output-pane");
  if(outputpane){
    outputpane.style.pointerEvents = "none";
  }
  // Disable transitions (reset in onMouseUp)
  document.body.classList.add('resizing');

  const parent = resizer.parentNode;
  const parentStyle = getComputedStyle(parent);
  if (parentStyle.display !== "flex") {
      return;
  }

  function findSibling(element, direction) {
    let sibling = direction === 'previous' ? element.previousElementSibling : element.nextElementSibling;    
    while (sibling) {
      if (!sibling.classList.contains("hidden") && !sibling.classList.contains("flex-resizer")) {
        return sibling;
      }
      sibling = direction === 'previous' ? sibling.previousElementSibling : sibling.nextElementSibling;
    }
    return null;
  }

  const direction = parentStyle['flex-direction'];
  const [prev, next, sizeProp, posProp] =
      direction === 'row'            ? [findSibling(resizer, 'previous'), findSibling(resizer, 'next'), "offsetWidth",  "pageX"] :
      direction === 'column'         ? [findSibling(resizer, 'previous'), findSibling(resizer, 'next'), "offsetHeight", "pageY"] :
      direction === 'row-reverse'    ? [findSibling(resizer, 'next'), findSibling(resizer, 'previous'), "offsetWidth",  "pageX"] :
      direction === 'column-reverse' ? [findSibling(resizer, 'next'), findSibling(resizer, 'previous'), "offsetHeight", "pageY"] :
      console.error(`internal error: unimplemented resizer for ${direction}`);

  // Avoid cursor flickering (reset in onMouseUp)
  document.body.style.cursor = getComputedStyle(resizer).cursor;

  let prevSize = prev[sizeProp];
  let nextSize = next[sizeProp];
  
  const sumSize = prevSize + nextSize;
  const prevGrow = Number(getComputedStyle(prev).flexGrow);
  const nextGrow = Number(getComputedStyle(next).flexGrow);

  const sumGrow = prevGrow + nextGrow;
  let lastPos = mouseDown[posProp];

  function onMouseMove(mouseMove) {
    
      let pos = mouseMove[posProp];
      const d = pos - lastPos;
      prevSize += d;
      nextSize -= d;
      if (prevSize < 0) {
          nextSize += prevSize;
          pos -= prevSize;
          prevSize = 0;
      }
      if (nextSize < 0) {
          prevSize += nextSize;
          pos += nextSize;
          nextSize = 0;
      }

      const prevGrowNew = sumGrow * (prevSize / sumSize);
      const nextGrowNew = sumGrow * (nextSize / sumSize);

      prev.style.flexGrow = prevGrowNew;
      next.style.flexGrow = nextGrowNew;

      lastPos = pos;
  }

  function onMouseUp(_mouseUp) {
      // Reset cursor state that was used to avoid cursor flickering
      document.body.style.removeProperty("cursor");
      // Enable output pane (iframe)
      const outputpane = document.querySelector("#output-pane");
      if(outputpane){
          outputpane.style.pointerEvents = "unset";
      }
      // (re)enable transitions
      document.body.classList.remove('resizing');

      window.removeEventListener("mousemove", onMouseMove);
      window.removeEventListener("mouseup", onMouseUp);
  }

  window.addEventListener("mousemove", onMouseMove);
  window.addEventListener("mouseup", onMouseUp);
}

document.body.addEventListener("mousedown", manageResize);
