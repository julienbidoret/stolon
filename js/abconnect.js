

function draw(from, to, update) {
  var svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
  svg.classList.add('line')
  document.body.appendChild(svg);
  let padding = 10;

  var vec1 = from.getBoundingClientRect();
  var vec2 = to.getBoundingClientRect();
  let top = Math.min(vec1.y, vec2.y) + window.scrollY;
  let left = Math.min(vec1.x, vec2.x);
  let width  = Math.floor(Math.max(40, Math.max(vec1.left, vec2.left) - Math.min(vec1.x, vec2.x))) + padding * 2
  let height = Math.floor(Math.max(40, Math.max(vec1.top, vec2.top) - Math.min(vec1.y, vec2.y))) + padding * 2
  
  const path = document.createElementNS("http://www.w3.org/2000/svg","path")

  svg.style.top = top + "px";
  svg.style.left = left + "px";
  svg.style.width = width + "px";
  svg.setAttribute("width", width);
  svg.style.height = height + "px";
  svg.setAttribute("height", height) ;
  svg.setAttribute("viewbox", `0 0 ${width} ${height}`);

  let fromPoint = {
    'left': vec1.x > vec2.x ? width - padding : padding,
    'top': vec1.y > vec2.y ? height - padding : padding  
  };
  let toPoint = {
    'left': vec1.x < vec2.x ? width - padding : padding,
    'top': vec1.y < vec2.y ? height - padding : padding 
  };
  let bezierPoint = {
    'left': vec1.y < vec2.y ? width - padding : width / 2,
    'top': vec1.x < vec2.x ? height - padding : height / 2
  }

  let d = `M ${fromPoint.left} ${fromPoint.top} Q ${bezierPoint.left} ${bezierPoint.top} ${toPoint.left} ${toPoint.top}`
  path.setAttribute('d', d);

  // path.style.stroke = vec1.y > vec2.y ? "red": "black"
  
  // randomly rotate
  if(Math.random() > .5 ){
    // durty fix margin on horizontal and vertical stolons
    if(vec1.y == vec2.y) svg.style.marginTop = "-40px"
    if(vec1.x == vec2.x) svg.style.marginLeft = "-40px"
    svg.style.setProperty("--rotate", "180deg")
  }

  svg.appendChild(path);
  // console.log(from, to, svg);
  // console.log(d);
}

function abconnect(from, to){
  const fromAnchor = from.querySelector(".title")
  const toAnchor = to.querySelector(".title")
  draw(fromAnchor, toAnchor, true);
}
