<?php require("functions.php"); ?>
<!DOCTYPE html>
<html>
<head>
  <title>Stolon</title>
  <link rel="stylesheet" href="css/reset.css">
  <link rel="stylesheet" href="font/stylesheet.css">
  <link rel="stylesheet" href="css/main.css">
  <link rel="stylesheet" href="js/monaco-editor/min/vs/editor/editor.main.css" data-name="vs/editor/editor.main" />
  <link href='img/favicon.png' rel='icon' type='image/png'>
</head>
    <?php
        
        // get title from url, see .htaccess
        if(!isset($_GET['title'])) { // no title found
          // new project: generate a new title
          $slickTitle = new SlickPassword();
          $title = $slickTitle->generateUS (8, 0, 0);
          $newTemplate = isset($_GET['template']) ? "?" . $_GET['template'] : "";
          header("LOCATION: ".$title.$newTemplate);
        } else {
          $title = $_GET['title'];
        }
        $stem_dir = "stems/".$title;
        
        // auth status
        $auth_classname = isset($_SESSION["stolon_username"]) ? "logged" : "anonymous";
        $username = isset($_SESSION["stolon_username"]) ? $_SESSION["stolon_username"] : "";

        // prepare fork
        $slickForkTitle = new SlickPassword();
        $forkTitle = $slickForkTitle->generateUS (8, 0, 0);

        // New stem or existing one ?
        if (!file_exists($stem_dir)) { // Creation of a temporary stem
          $t = isset($_GET["template"]) ? $_GET["template"] : "default";
          $stem_dir = getTemplate($t);                 
          $saved_classname = "";
        } else { // existing project
          $saved_classname = "saved";          
        }

        // templates
        $all_templates = getTemplates();
        
        // Decode json
        $json_data = file_get_contents($stem_dir."/stem.json");
        $j = json_decode($json_data, true);

        ?>
        <body class="<?= $saved_classname ?> <?= $auth_classname ?>"
          data-stem_dir="<?= $stem_dir ?>"
          data-livereload="<?= $j['config']['liveReload'] ?>"
          data-layout="<?= $j['config']['layoutMode'] ?>" >
        <header>
          <form class="group" id="mainform" method='POST' action='write.php' onsubmit='submitForm();' >
            <label>title <input class='title' type='text' name='title' value='<?= $title ?>'/></label>
            <input hidden id="original-title" type="text" name="original_title" value="<?= $title ?>">
            <input hidden id="derivated-from" type="text" name="derivated_from" value="<?= $j['derivated_from'] ?>">
            <input hidden id="live-reload" type="text" name="live_reload" value="false">
            <input hidden id="fork-title" type="text" name="fork_title" value="<?= $forkTitle ?>">            
          </form>
          <div class="group">
            <span title="Save (CTRL + S)" class='save-btn button'>save</span>
            <span class="buttons-group">
              <a title="Create a new stem" class="button buttons-group-first" href="./">new</a>
              <button class="button buttons-group-last" id="templates-dropdown-button"><svg class="icon" viewbox="0 0 10 10"><path d="M 2 4 L 5 8, 8 4" /></svg></button>
            </span>
            <span title="Create a new stem from the current one" class="button fork-btn">fork</span>
            <button title="Download this stem’s files as a zip" class="download-btn button" data-url="download.php?stem=<?= $title ?>">download</button>
          </div>
          <div class="group">
            <button class="button toggle" data-toggle="html">html</button>
            <button class="button toggle" data-toggle="css">css</button>
            <button class="button toggle" data-toggle="js">js</button>
            <button class="button toggle" data-toggle="output">output</button>
          </div>
          <div class="group ">
            <a class="button" href="list.php">gallery</a>
            <button class="menu-btn button">config</button>          
            <a class="button login-btn" href="#login-form" >login</a>
            <form id="login-form" method="post" target="_self">
              <p><label for="stolon_username">Username</label><input type="text" name="stolon_username" id="stolon_username" required></p>
              <p><label for="password">Password</label><input type="password" name="password" required></p>
              <input class="button" type="submit" value="login">
              <button class="button" type="reset" id="cancel-login">×</button>
            </form>          
            <form id="logout-form" method="post" target="_self">
              <input type="hidden" name="logout" value="1">
              <input class="button logout" type="submit" value="" title="logout <?= $username ?>">
            </form>
          </div>
          <div class="menu-container">
            <div class="output-link-line menu-line"><a target="_blank" href="<?= $stem_dir ?>/index.html">➚ link to output</a>
            </div>
            <hr class="menu-line">
            <label class="fs-line menu-line">
              <span>font size</span>
              <input class="editable" type="range" id="fs" min="5" max="30" value="15" step="1">
            </label>
            <label class="live-line menu-line">
              <span>live reload</span>
              <span class="editable">
                <input type="checkbox">
              </span>
            </label>
            <div class="layout-line menu-line">
              <span>layout mode</span>
              <div class="editable">
                <button data-layout="vertical" title="vertical"></button>
                <button data-layout="vertical-split" title="vertical split"></button>
                <button data-layout="horizontal" title="horizontal"></button>
                <button data-layout="horizontal-split" title="horizotal split"></button>
              </div>
            </div>
            <div class="theme-line menu-line">
              <label for="theme">theme</label>
              <select name="theme" id="theme" class="editable">
                <?php foreach ($themes as $t) :?>
                  <option value="<?= $t ?>" <?= $t == $my_theme ? "selected" : "" ?>><?= $t ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <label class="screenshot-line menu-line">
              <span>screenshot</span>
              <span class="editable">
                <button id="take-screenshot" class="button">take screenshot</button>
              </span>
            </label>
            <hr class="menu-line">
            <div class="menu-line"><a target="_blank" href="https://gitlab.com/raphaelbastide/stolon">★ info and code</a></div>
          </div>
          <div class="templates-container">
            <?php foreach($all_templates as $template => $dir) :?>
              <a class="button" href="./?template=<?= $template ?>"><?= $template ?></a>
            <?php endforeach ?>
          </div>
      </header>
        <div class='container' id="main">
          <div id="editors">
            <div id="html-pane" class="pane html-pane">
              <div id='html-editor' class="editor">
              </div>
            </div>
            <hr class="flex-resizer">
            <div id="css-pane" class='pane css-pane'>
              <div id='css-editor' class="editor">
              </div>
            </div>
            <hr class="flex-resizer">
            <div id="js-pane" class='pane js-pane'>
              <div id='js-editor' class="editor">
              </div>
            </div>
          </div>
          <hr class="flex-resizer">
            <div id="output-pane" class='pane output-pane'>
              <iframe id="output" class="output" data-src="<?= $stem_dir ?>/index.html" src=""></iframe>
            </div>
        </div>
        <div id="upload">
          <label for="screenshot">Take a screenshot of the rendering, locate the file, select and upload it.</label>
          <input type="file" id="screenshot" name="screenshot" accept="image/png" >
          <input type="button" class="button" value="Upload" id="upload-btn">
        </div>

        <script src="js/cookies.js"></script>
        <script src="js/slug.js"></script>
        <script src="js/monaco-editor/min/vs/loader.js"></script>
        <script src="js/emmet-monaco.min.js" ></script>
        <script src="js/main.js" ></script>
        <script src="js/resizer.js"></script>
      </body>
</html>
