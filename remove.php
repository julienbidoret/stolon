<?php

session_start();

$title            = $_POST["stem"];
$project_dir      = "stems/".$title;

$response = [
  "success" => 0,
  "auth" => false,
  "messages" => []
];

// authenticate
if (isset($_SESSION["stolon_username"])) { 
  rrmdir($project_dir);
  $response["success"] = 1;
  $response["auth"] = true;
  $response["messages"][] = "$title has been removed!";
} else {
  $response["success"] = 0;
  $response["auth"] = false;
  $response["messages"][] = "Error: Wrong password, can’t remove $title!";
}

echo json_encode($response);

function rrmdir($dir) { 
  if (is_dir($dir)) { 
    $objects = scandir($dir);
    foreach ($objects as $object) { 
      if ($object != "." && $object != "..") { 
        if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
          rrmdir($dir. DIRECTORY_SEPARATOR .$object);
        else
          unlink($dir. DIRECTORY_SEPARATOR .$object); 
      } 
    }
    rmdir($dir); 
  } 
}

?>
